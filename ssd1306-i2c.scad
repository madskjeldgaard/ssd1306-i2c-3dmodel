/*

This is a 3d model of the ssd1306 four pin i2c screen module you can get everywhere.

NOTE: The pin header holes are not super precise.

*/

$fn=100;

pcb_w=25.94;
pcb_h=25.91;
difference(){
	// PCB
	difference() {
		color("blue") cube([pcb_h, pcb_w, 1.6],center=true);

		// pin holes
		for (i=[0:3]){
			color("yellow") translate([(-pcb_h*0.5)+2,0.8+(-pcb_w*0.5)+7.5+(i*3.0), -3]) cylinder(h=4, d=1.6,center = false);
		}
	}

	// Mounting holes
	mount_dia=1.99;
	translate([0,0,-1]){
		translate([-pcb_h*0.5+1.5,-pcb_w*0.5+1.5,0]) cylinder(h=4, d=mount_dia,center = false);
		translate([pcb_h*0.5-1.5,pcb_w*0.5-1.5,0]) cylinder(h=4, d=mount_dia,center = false);
		translate([pcb_h*0.5-1.5,-pcb_w*0.5+1.5,0]) cylinder(h=4, d=mount_dia,center = false);
		translate([-pcb_h*0.5+1.5,pcb_w*0.5-1.5,0]) cylinder(h=4, d=mount_dia,center = false);
	}

}
// Screen
translate([-pcb_h*0.5+5.1,-pcb_w*0.5,0.8]) color("silver") cube([16.9, 25.94, 1.6],center=false);
